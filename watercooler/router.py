import logging

from django.conf import settings

import zmq
from zmq.eventloop import zmqstream

from watercooler.utils import json


__all__ = [
    'get_socket',
    'Router',
    'Publisher',
    'SyncPublisher',
    'Subscriber',
    'Processor'
]


logger = logging.getLogger(__name__)


_context = None

def get_socket(socket_type):
    global _context
    if _context is None:
        _context = zmq.Context()
    return _context.socket(socket_type)


class Router(object):
    def on_message(self, message):
        logger.debug("%s.on_message %r", type(self).__name__, message)
        self.stream_out.send_multipart(message)

    def start(self):
        socket_in = get_socket(zmq.PULL)
        socket_in.bind('tcp://{}:{}'.format(settings.ROUTER_ADDRESS, settings.ROUTER_PORT_IN))
        stream_in = zmqstream.ZMQStream(socket_in)
        stream_in.on_recv(self.on_message)
        self.stream_in = stream_in
        socket_out = get_socket(zmq.PUB)
        socket_out.bind('tcp://{}:{}'.format(settings.ROUTER_ADDRESS, settings.ROUTER_PORT_OUT))
        self.stream_out = zmqstream.ZMQStream(socket_out)


class Publisher(object):
    def __repr__(self):
        return u"{0}()".format(type(self).__name__)

    def send(self, key, data):
        try:
            stream_out = self.stream_out
        except AttributeError:
            socket_out = get_socket(zmq.PUSH)
            socket_out.connect('tcp://{}:{}'.format(settings.ROUTER_ADDRESS, settings.ROUTER_PORT_IN))
            stream_out = zmqstream.ZMQStream(socket_out)
            self.stream_out = stream_out
        logger.debug("%s.send %r %r", type(self).__name__, key, data)
        stream_out.send_multipart((key, json.dumps(data)))

    def stop(self):
        try:
            self.stream_out.close()
            del self.stream_out
        except AttributeError:
            pass


class SyncPublisher(Publisher):
    def send(self, key, data):
        super(SyncPublisher, self).send(key, data)
        self.stream_out.flush()


class Subscriber(object):
    keys = set()

    def __init__(self, keys=None):
        if keys is None:
            self.keys = list(self.keys)
        else:
            if isinstance(keys, basestring):
                keys = [keys]
            self.keys = set(keys)

    def __repr__(self):
        return u"{0}({1.keys!r})".format(type(self).__name__, self)

    def _on_message(self, message):
        logger.debug("%s._on_message %r", type(self).__name__, message)
        self.receive(message[0], json.loads(message[1]))

    def receive(self, key, data):
        logger.debug("%s.receive %r %r", type(self).__name__, key, data)

    def subscribe(self, key):
        self.stream_in.setsockopt(zmq.SUBSCRIBE, key)
        self.keys.add(key)

    def unsubscribe(self, key):
        self.stream_in.setsockopt(zmq.UNSUBSCRIBE, key)
        self.keys.remove(key)

    def start(self):
        socket_in = get_socket(zmq.SUB)
        socket_in.connect('tcp://{}:{}'.format(settings.ROUTER_ADDRESS, settings.ROUTER_PORT_OUT))
        for key in self.keys:
            socket_in.setsockopt(zmq.SUBSCRIBE, key)
        stream_in = zmqstream.ZMQStream(socket_in)
        stream_in.on_recv(self._on_message)
        self.stream_in = stream_in

    def stop(self):
        try:
            self.stream_in.close()
            del self.stream_in
        except AttributeError:
            pass


class Processor(Publisher, Subscriber):
    def stop(self):
        Publisher.stop(self)
        Subscriber.stop(self)
