from django.contrib import admin
from django.utils.translation import ugettext_lazy as _

from watercooler.models import Account, Message


class AccountAdmin(admin.ModelAdmin):
    list_display = ['admin_unicode', 'name']
    ordering = ['-id']

    def admin_unicode(self, account):
        return u"#{}".format(account.id)
    admin_unicode.admin_order_field = 'id'
    admin_unicode.short_description = _(u"account")

admin.site.register(Account, AccountAdmin)


class MessageAdmin(admin.ModelAdmin):
    date_hierarchy = 'time'
    list_display = ['admin_unicode', 'account', 'text', 'time']
    list_filter = ['time']
    ordering = ['-id']

    def admin_unicode(self, message):
        return u"#{}".format(message.id)
    admin_unicode.admin_order_field = 'id'
    admin_unicode.short_description = _(u"message")

admin.site.register(Message, MessageAdmin)
