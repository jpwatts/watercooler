import datetime
import importlib
import logging

from django.conf import settings

import tornado.websocket

from watercooler.router import Processor
from watercooler.middleware import SESSION_KEY
from watercooler.models import Account, Message
from watercooler.utils import json


logger = logging.getLogger(__name__)


_session_engine = importlib.import_module(settings.SESSION_ENGINE)

class AccountWebSocketHandler(tornado.websocket.WebSocketHandler):
    def _execute(self, transforms, *args, **kwargs):
        if self.account is None:
            self.stream.write("HTTP/1.1 403 Forbidden\r\n\r\n")
            self.stream.close()
            return
        super(AccountWebSocketHandler, self)._execute(transforms, *args, **kwargs)

    def on_message(self, message):
        try:
            data = json.loads(message)
        except ValueError:
            self.write_error(u"This message doesn't contain a JSON object.")
            self.close()
            return
        self.on_data(data)

    def on_data(self, data):
        logger.info(u"AccountWebSocketHandler.on_data %r", data)

    def write_data(self, key, data):
        self.write_message(json.dumps({'key': key, 'data': data}))

    def write_error(self, errors):
        if isinstance(errors, basestring):
            errors = {'__all__': [errors]}
        self.write_data('error', errors)

    @property
    def account(self):
        try:
            return self._account
        except AttributeError:
            pass
        session_key = self.get_cookie(settings.SESSION_COOKIE_NAME)
        account = None
        if session_key is not None:
            session = _session_engine.SessionStore(session_key)
            try:
                account = Account.objects.get(id=session[SESSION_KEY])
            except (KeyError, Account.DoesNotExist):
                pass
        self._account = account
        return account


class ChatProcessor(Processor):
    keys = [
        'watercooler.account.',
        'watercooler.message.'
    ]

    def __init__(self, handler):
        super(ChatProcessor, self).__init__()
        self.handler = handler

    def receive(self, key, data):
        logger.debug(u"ChatProcessor.receive %r %r %r", self.handler.account, key, data)
        self.handler.write_data(key, data)


class ChatWebSocketHandler(AccountWebSocketHandler):
    def open(self):
        logger.info(u"ChatWebSocketHandler.open %r", self.account)
        processor = ChatProcessor(self)
        processor.start()
        self.processor = processor
        self.backfill()

    def backfill(self, hours=24, limit=100):
        messages = Message.objects.filter(
            time__gte=datetime.datetime.utcnow() - datetime.timedelta(hours=hours)
        ).order_by('-id')[:limit]
        accounts = set(m.account for m in messages)
        for account in accounts:
            self.write_data('watercooler.account.backfill', account)
        for message in messages:
            self.write_data('watercooler.message.backfill', message)

    def on_close(self):
        logger.info(u"ChatWebSocketHandler.on_close %r", self.account)
        self.processor.stop()
        del self.processor

    def on_data(self, data):
        logger.debug(u"ChatWebSocketHandler.on_data %r %r", self.account, data)
        try:
            self.processor.send(data['key'].encode('utf-8'), data['data'])
        except (AttributeError, KeyError):
            self.write_error(u"This message can't be processed.")
            self.close()
            return
