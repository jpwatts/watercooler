from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.contrib.staticfiles.urls import staticfiles_urlpatterns


admin.autodiscover()


urlpatterns = patterns('watercooler.views',
    url(r'^$', 'app_view', name='app'),
    url(r'^message/(?:\d+)$', 'app_view', name='app_message'),
    url(r'^_api/account/$', 'account_collection', name='api_account_collection'),
    url(r'^_api/account/(?P<account_id>\d+)$', 'account_resource', name='api_account_resource'),
    url(r'^_api/auth$', 'auth_resource', name='api_auth_resource'),
    url(r'^_api/message/$', 'message_collection', name='api_message_collection'),
    url(r'^_api/message/(?P<message_id>\d+)$', 'message_resource', name='api_message_resource'),
    url(r'^_admin/doc/', include('django.contrib.admindocs.urls')),
    url(r'^_admin/', include(admin.site.urls)),
)


urlpatterns += staticfiles_urlpatterns()
