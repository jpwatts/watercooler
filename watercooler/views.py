from django import http
from django.views.generic import TemplateView

from watercooler.forms import AccountForm, MessageForm
from watercooler.middleware import SESSION_KEY
from watercooler.models import Account, Message
from watercooler.utils import api


class AppView(TemplateView):
    template_name = 'app.html'

app_view = AppView.as_view()


@api.view(['GET', 'HEAD', 'POST'])
def account_collection(request):
    if request.method == 'POST':
        form = AccountForm(request.data)
        if not form.is_valid():
            return api.error(request, form.errors, status=409)
        account = request.account
        if account is None:
            request.session.cycle_key()
        else:
            request.session.flush()
        account = form.save()
        request.session[SESSION_KEY] = account.id
        request.account = account
        response = api.response(request, account, status=201)
        response['Location'] = './{}'.format(account.id)
        return response
    return api.response(request, {'objects': []})


@api.view(['DELETE', 'GET', 'HEAD', 'PUT'])
def account_resource(request, account_id):
    try:
        account = Account.objects.get(id=account_id)
    except Account.DoesNotExist:
        return api.error(request, u"This account doesn't exist.", status=404)
    if request.method in ['DELETE', 'PUT'] and request.account != account:
        return api.error(request, u"This account doesn't belong to you.", status=409)
    if request.method == 'DELETE':
        account.delete()
        return api.response(request, None, status=204)
    if request.method == 'PUT':
        form = AccountForm(request.data, instance=account)
        if not form.is_valid():
            return api.error(request, form.errors, status=409)
        account = form.save()
    return api.response(request, account)


@api.view(['DELETE', 'GET', 'HEAD'])
def auth_resource(request):
    if request.method == 'DELETE':
        request.session.flush()
        request.account = None
        return api.response(request, None, status=204)
    account = request.account
    if account is None:
        return api.error(request, u"You're not signed in.", status=404)
    return http.HttpResponseRedirect('./account/{}'.format(account.id))


@api.view(['GET', 'HEAD', 'POST'])
def message_collection(request):
    if request.method == 'POST':
        account = request.account
        if account is None:
            return api.error(request, u"You're not signed in.", status=409)
        form = MessageForm(request.data)
        if not form.is_valid():
            return api.error(request, form.errors, status=409)
        message = form.save(commit=False)
        message.account = account
        message.save()
        response = api.response(request, message, status=201)
        response['Location'] = './{}'.format(message.id)
        return response
    return api.response(request, {'objects': []})


@api.view(['DELETE', 'GET', 'HEAD', 'PUT'])
def message_resource(request, message_id):
    try:
        message = Message.objects.get(id=message_id)
    except Message.DoesNotExist:
        return api.error(request, u"This message doesn't exist.", status=404)
    if request.method in ['DELETE', 'PUT'] and request.account.id != message.account_id:
        return api.error(request, u"This message doesn't belong to you.", status=409)
    if request.method == 'DELETE':
        message.delete()
        return api.response(request, None, status=204)
    if request.method == 'PUT':
        form = MessageForm(request.data, instance=message)
        if not form.is_valid():
            return api.error(request, form.errors, status=409)
        message = form.save()
    return api.response(request, message)
