from watercooler.models import Account


SESSION_KEY = '_watercooler_account'


class LazyAccount(object):
    def __get__(self, request, owner=None):
        try:
            return request._account
        except AttributeError:
            pass
        try:
            account = Account.objects.get(id=request.session[SESSION_KEY])
        except (KeyError, Account.DoesNotExist):
            account = None
        request._account = account
        return account


class AccountMiddleware(object):
    def process_request(self, request):
        request.__class__.account = LazyAccount()
