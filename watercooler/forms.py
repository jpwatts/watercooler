from django import forms

from watercooler.models import Account, Message


class AccountForm(forms.ModelForm):
    class Meta(object):
        fields = ['name']
        model = Account


class MessageForm(forms.ModelForm):
    class Meta(object):
        fields = ['text']
        model = Message
