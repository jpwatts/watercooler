from django.db import models
from django.db.models.signals import post_save, post_delete
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _

from watercooler.router import Publisher


_publisher = Publisher()


class Account(models.Model):
    name = models.CharField(_(u"name"), max_length=50, blank=True)

    class Meta(object):
        verbose_name = _(u"account")
        verbose_name_plural = _(u"accounts")

    def __unicode__(self):
        name = self.name
        if not name:
            name = u"#{}".format(self.id)
        return name

    @classmethod
    def post_save(cls, sender, instance, created, **kwargs):
        _publisher.send('watercooler.account.{}'.format('created' if created else 'updated'), instance)

    @classmethod
    def post_delete(cls, sender, instance, **kwargs):
        _publisher.send('watercooler.account.deleted', instance)

    def dump(self):
        return dict(id=self.id, name=self.name)

post_save.connect(Account.post_save, sender=Account, dispatch_uid='watercooler.account.post_save')
post_delete.connect(Account.post_delete, sender=Account, dispatch_uid='watercooler.account.post_delete')


class Message(models.Model):
    account = models.ForeignKey('Account', verbose_name=_(u"account"), related_name='messages')
    text = models.TextField(_(u"text"), blank=True)
    time = models.DateTimeField(_(u"time"), default=timezone.now)

    class Meta(object):
        get_latest_by = 'time'
        verbose_name = _(u"message")
        verbose_name_plural = _(u"messages")

    def __unicode__(self):
        return self.text

    @classmethod
    def post_save(cls, sender, instance, created, **kwargs):
        _publisher.send('watercooler.message.{}'.format('created' if created else 'updated'), instance)

    @classmethod
    def post_delete(cls, sender, instance, **kwargs):
        _publisher.send('watercooler.message.deleted', instance)

    def dump(self):
        return dict(id=self.id, account=self.account_id, text=self.text, time=self.time)

post_save.connect(Message.post_save, sender=Message, dispatch_uid='watercooler.message.post_save')
post_delete.connect(Message.post_delete, sender=Message, dispatch_uid='watercooler.message.post_delete')
