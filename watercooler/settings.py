import os


PROJECT_ROOT = os.path.realpath(os.path.join(os.path.dirname(__file__), os.pardir))
ROOT = os.environ['VIRTUAL_ENV']

DEBUG = True
TEMPLATE_DEBUG = DEBUG

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'watercooler',
        'USER': 'app',
    }
}

TIME_ZONE = 'Etc/UTC'

LANGUAGE_CODE = 'en-us'

SITE_ID = 1

USE_I18N = True

USE_L10N = True

USE_TZ = True

MEDIA_ROOT = os.path.join(ROOT, 'data/media')

MEDIA_URL = '/_media/'

STATIC_ROOT = os.path.join(ROOT, 'data/static')

STATIC_URL = '/_static/'

STATICFILES_DIRS = (
    os.path.join(PROJECT_ROOT, 'static'),
)

SECRET_KEY = 'g&amp;8in)p+d=eartvsgdno=5clpu-amtm3-01_@b9ej1g5t26%jw'

MIDDLEWARE_CLASSES = (
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'watercooler.middleware.AccountMiddleware',
)

ROOT_URLCONF = 'watercooler.urls'

WSGI_APPLICATION = 'watercooler.wsgi.application'

TEMPLATE_DIRS = (
    os.path.join(PROJECT_ROOT, 'templates'),
)

INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.admin',
    'django.contrib.admindocs',
    'watercooler',
)

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        }
    },
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler'
        }
    },
    'loggers': {
        'django.request': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': True,
        },
    }
}

ROUTER_ADDRESS = '127.0.0.1'
ROUTER_PORT_IN = 9998
ROUTER_PORT_OUT = 9999

WEB_ADDRESS = '127.0.0.1'
WEB_PORT = 8000


try:
    from watercooler.settings_local import *
except ImportError:
    pass
