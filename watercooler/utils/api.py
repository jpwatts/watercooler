import functools

from django import http
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import require_http_methods

from watercooler.utils import json


__all__ = [
    'response',
    'error',
    'view'
]

def response(request, data, **kwargs):
    kwargs.setdefault('content_type', 'application/json')
    status = kwargs.setdefault('status', 200)
    if request.method == 'HEAD' or status == 204:
        content = ''
    else:
        content = json.dumps(data)
    return http.HttpResponse(content, **kwargs)


def error(request, errors, **kwargs):
    kwargs.setdefault('status', 400)
    if isinstance(errors, basestring):
        errors = {'__all__': [errors]}
    return response(request, {'errors': errors}, **kwargs)


def view(methods):
    def decorator(view_func):
        @csrf_exempt
        @require_http_methods(methods)
        @functools.wraps(view_func)
        def wrapper(request, *args, **kwargs):
            if request.method in ['PUT', 'POST']:
                try:
                    request.data = json.loads(request.body)
                except ValueError:
                    return error(request, u"This request doesn't contain a JSON object.")
            return view_func(request, *args, **kwargs)
        return wrapper
    return decorator
