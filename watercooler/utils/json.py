from __future__ import absolute_import

import calendar
import datetime

from django.db.models.query import QuerySet

try:
    import simplejson as json
except ImportError:
    import json


__all__ = [
    'JSONEncoder',
    'dump',
    'dumps',
    'load',
    'loads',
]


class JSONEncoder(json.JSONEncoder):
    def default(self, obj):
        try:
            return obj.dump()
        except AttributeError:
            pass
        if isinstance(obj, QuerySet):
            return list(obj)
        if isinstance(obj, datetime.datetime):
            return calendar.timegm(obj.utctimetuple())
        return super(JSONEncoder, self).default(obj)


def dump(obj, f):
    return json.dumps(obj, f, cls=JSONEncoder, separators=(',', ':'), sort_keys=True)


def dumps(obj):
    return json.dumps(obj, cls=JSONEncoder, separators=(',', ':'), sort_keys=True)


load = json.load
loads = json.loads
