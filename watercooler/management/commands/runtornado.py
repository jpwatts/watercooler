import logging

from django.conf import settings
from django.core.management.base import BaseCommand, CommandError
from django.core.handlers.wsgi import WSGIHandler

# Install ZMQ IOLoop before touching Tornado.
from zmq.eventloop import ioloop
ioloop.install()

import tornado.autoreload
import tornado.web
import tornado.wsgi

from watercooler import router, websocket


logger = logging.getLogger(__name__)


class ChatSubscriber(router.Subscriber):
    keys = [
        'watercooler.account.',
        'watercooler.message.',
    ]

    def receive(self, key, data):
        logger.info(u"ChatSubscriber.receive %r %r", key, data)


class Command(BaseCommand):
    def handle(self, *args, **options):
        router.Router().start()
        ChatSubscriber().start()
        application = tornado.web.Application([
            (r'^/_socket', websocket.ChatWebSocketHandler),
            (r'^/.*', tornado.web.FallbackHandler, {
                'fallback': tornado.wsgi.WSGIContainer(WSGIHandler())
            })
        ])
        application.listen(settings.WEB_PORT, settings.WEB_ADDRESS)
        tornado.autoreload.start()
        ioloop.IOLoop.instance().start()
