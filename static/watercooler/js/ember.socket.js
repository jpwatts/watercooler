;(function() {

var Socket = Ember.Namespace.create();


Socket.RECONNECT_TIMEOUT = 10000; // ten seconds


Socket.set('isAvailable', navigator.onLine);
$(window).bind('online offline', function(e) {
    Socket.set('isAvailable', navigator.onLine);
});


Socket.Socket = Ember.Object.extend({

    isConnected: false,
    _shouldReconnect: false,

    _connect: function() {
        // console.debug("%@._connect".fmt(this));

        var socket = this,
            _reconnectTimeout = this.get('_reconnectTimeout'),
            _socket = new WebSocket(this.get('url'));

        this._cancelReconnect();

        _socket.onopen = function(e) {
            // console.debug("%@.onopen".fmt(socket), e);
            socket.set('isConnected', true);
        };

        _socket.onmessage = function(e) {
            // console.debug("%@.onmessage".fmt(socket), e);
            socket.receive(JSON.parse(e.data));
        };

        _socket.onclose = function(e) {
            // console.debug("%@.onclose".fmt(socket), e);
            socket.set('isConnected', false);
            if (socket.get('_shouldReconnect')) {
                socket._scheduleReconnect();
            }
        };

        _socket.onerror = function(e) {
            console.error("%@.onerror".fmt(socket), e);
        };

        this.set('_socket', _socket);
    },

    _disconnect: function() {
        // console.debug("%@._disconnect".fmt(this));
        var _socket = this.get('_socket');
        if ('undefined' !== typeof _socket) {
            _socket.close();
            this.set('_socket', undefined);
        }
    },

    _cancelReconnect: function() {
        // console.debug("%@._cancelReconnect".fmt(this));
        var _reconnectTimeout = this.get('_reconnectTimeout');
        if ('undefined' !== typeof _reconnectTimeout) {
            clearTimeout(_reconnectTimeout);
            this.set('_reconnectTimeout', undefined);
            console.info("%@ cancelled reconnect".fmt(this), _reconnectTimeout);
        }
    },

    _scheduleReconnect: function() {
        // console.debug("%@._scheduleReconnect".fmt(this));
        this._cancelReconnect();
        var socket = this,
            _reconnectTimeout = setTimeout(function() {
                socket.set('_reconnectTimeout', undefined);
                socket.connect();
            }, Socket.RECONNECT_TIMEOUT);
        console.info("%@ scheduled reconnect".fmt(this), _reconnectTimeout);
        this.set('_reconnectTimeout', _reconnectTimeout);
    },

    _availabilityChanged: function() {
        // console.debug("%@._availabilityChanged".fmt(this));
        if (Socket.get('isAvailable')) {
            if (this.get('_shouldReconnect')) {
                this._connect();
            }
        } else {
            this._cancelReconnect();
            this._disconnect();
        }
    }.observes('Socket.isAvailable'),

    connect: function() {
        console.debug("%@.connect".fmt(this));
        this.set('_shouldReconnect', true);
        if (Socket.get('isAvailable')) {
            this._connect();
        }
    },

    disconnect: function() {
        console.debug("%@.disconnect".fmt(this));
        this.set('_shouldReconnect', false);
        this._cancelReconnect();
        this._disconnect();
    },

    reconnect: function() {
        console.debug("%@.reconnect".fmt(this));
        this._disconnect();
        this.connect();
    },

    receive: function(data) {
        console.info("%@.receive".fmt(this), data);
    },
    
    send: function(data) {
        console.debug("%@.send".fmt(this), data);
        this.get('_socket').send(JSON.stringify(data));
    }

});


window.Socket = Socket;

})();
