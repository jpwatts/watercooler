var App = Ember.Application.create();


App.Account = Ember.Object.extend({
    description: function() {
        return this.get('name') || this.get('number');
    }.property('name', 'number'),
    number: function() {
        return "#%@".fmt(this.get('id'));
    }.property('id'),
    getData: function() {
        var data = {
            id: this.get('id'),
            name: this.get('name')
        };
        return data;
    },
    setData: function(data) {
        this.setProperties(data);
    }
});

App.Account.reopenClass({
    _cache: {},
    find: function(accountId, data) {
        var account = this._cache[accountId];
        if ('undefined' === typeof account) {
            account = this.create({id: accountId});
            if ('undefined' === typeof data) {
                $.ajax({
                    url: '/_api/account/%@'.fmt(accountId),
                    success: function(data) {
                        account.setData(data);
                    }
                });
            } else {
                account.setData(data);
            }
            this._cache[accountId] = account;
        }
        return account;
    }
});


App.Message = Ember.Object.extend({
    number: function() {
        return "#%@".fmt(this.get('id'));
    }.property('id'),
    timeText: function() {
        var time = this.get('time');
        return time ? time.toLocaleString() : "";
    }.property('time'),
    timeISOText: function() {
        var time = this.get('time');
        return time ? time.toISOString() : "";
    }.property('time'),
    getData: function() {
        var data = {
            id: this.get('id'),
            account: this.get('account.id'),
            text: this.get('text')
        };
        var time = this.get('time');
        data.time = time && time.getTime() / 1000;
        return data;
    },
    setData: function(data) {
        data = Ember.copy(data);
        var account = data.account;
        if ('undefined' !== typeof account) {
            data.account = App.Account.find(account);
        }
        var time = data.time;
        if ('undefined' !== typeof time) {
            data.time = new Date(data.time * 1000);
        }
        this.setProperties(data);
    }
});

App.Message.reopenClass({
    _cache: {},
    find: function(messageId, data) {
        var message = this._cache[messageId];
        if ('undefined' === typeof message) {
            message = this.create({id: messageId});
            if ('undefined' === typeof data) {
                $.ajax({
                    url: '/_api/message/%@'.fmt(messageId),
                    success: function(data) {
                        message.setData(data);
                    }
                });
            } else {
                message.setData(data);
            }
            this._cache[messageId] = message;
        }
        return message;
    }
});


App._handleAccountBackfillCreatedUpdatedUpdating = function(data) {
//    console.debug("_handleAccountCreatedUpdatedUpdating", data);
    App.Account.find(data.id, data).setData(data);
};
App._handleAccountDeleted = function(data) {
//    console.debug("_handleAccountDeleted", data);
    App.Account.find(data.id, data).destroy();
};

App._handleMessageCreatedUpdatedUpdating = function(data) {
//    console.debug("_handleMessageCreatedUpdatedUpdating", data);
    var controller = App.messageListController,
        message = App.Message.find(data.id, data);
    message.setData(data);
    if (App.get('authController.account') !== message.get('account') && !controller.contains(message)) {
        controller.unshiftObject(message);
        controller.sortContent();
    }
};
App._handleMessageBackfill = function(data) {
//    console.debug("_handleMessageBackfill", data);
    var controller = App.messageListController,
        message = App.Message.find(data.id, data);
    message.setData(data);
    if (!controller.contains(message)) {
        controller.pushObject(message);
        controller.sortContent();
    }
};
App._handleMessageDeleted = function(data) {
//    console.debug("_handleMessageDeleted", data);
    var controller = App.messageListController,
        message = App.Message.find(data.id, data);
    controller.removeObject(message);
    message.destroy();
};

App.socket = Socket.Socket.create({
    url: 'ws://%@/_socket'.fmt(window.location.host),
    receive: function(message) {
        console.info("%@.receive".fmt(this), message);
        var handler = {
            'watercooler.account.backfill': App._handleAccountBackfillCreatedUpdatedUpdating,
            'watercooler.account.created': App._handleAccountBackfillCreatedUpdatedUpdating,
            'watercooler.account.deleted': App._handleAccountDeleted,
            'watercooler.account.updating': App._handleAccountBackfillCreatedUpdatedUpdating,
            'watercooler.account.updated': App._handleAccountBackfillCreatedUpdatedUpdating,
            'watercooler.message.backfill': App._handleMessageBackfill,
            'watercooler.message.created': App._handleMessageCreatedUpdatedUpdating,
            'watercooler.message.deleted': App._handleMessageDeleted,
            'watercooler.message.updating': App._handleMessageCreatedUpdatedUpdating,
            'watercooler.message.updated': App._handleMessageCreatedUpdatedUpdating
        }[message.key];
        if ('undefined' === typeof handler) {
            console.warn("unhandled message", message);
            return;
        }
        handler(message.data);
    }
});


App.authController = Ember.Controller.create({
    account: null,
    _signIn: function(data) {
        var account = App.Account.find(data.id, data);
        account.setData(data);
        this.set('account', account);
        App.socket.connect();
    },
    signIn: function() {
        console.debug("%@.signIn".fmt(this));
        if (this.get('account')) { return; }
        var controller = this;
        $.ajax({
            url: '/_api/auth',
            success: function(data) {
                controller._signIn(data);
            },
            error: function() {
                $.ajax({
                    url: '/_api/account/',
                    contentType: 'application/json',
                    data: "{}",
                    type: "POST",
                    success: function(data) {
                        controller._signIn(data);
                    }
                });
            }
        });
    },
    signOut: function() {
        console.debug("%@.signOut".fmt(this));
        var controller = this;
        $.ajax({
            url: '/_api/auth',
            type: "DELETE",
            success: function() {
                controller.set('account', null);
                App.socket.disconnect();
            }
        });
    },
    saveAccount: function(account) {
        console.debug("%@.saveAccount".fmt(this), account);
        $.ajax({
            url: '/_api/account/%@'.fmt(account.get('id')),
            contentType: 'application/json',
            data: JSON.stringify(account.getData()),
            type: "PUT"
        });
    },
    updateAccount: function(account) {
        console.debug("%@.updateAccount".fmt(this), account);
        App.socket.send({
            key: "watercooler.account.updating",
            data: account.getData()
        });
    }
});


App.messageListController = Ember.ArrayController.create({
    init: function() {
        this.set('content', []);
    },
    sortContent: function() {
        var content = this.get('content');
        this.set('content', content.sort(function(a, b) {
            return b.get('id') - a.get('id');
        }));
    },
    createMessage: function() {
        console.debug("%@.createMessage".fmt(this));
        var controller = this,
            message = App.Message.create({account: App.get('authController.account')});
        $.ajax({
            url: '/_api/message/',
            contentType: 'application/json',
            data: JSON.stringify(message.getData()),
            type: "POST",
            success: function(data) {
                delete data.text;
                message.setData(data);
                controller.unshiftObject(message);
            }
        });
        return message;
    },
    deleteMessage: function(message) {
        console.debug("%@.deleteMessage".fmt(this), message);
        var controller = this;
        $.ajax({
            url: '/_api/message/%@'.fmt(message.get('id')),
            type: "DELETE",
            success: function(data) {
                controller.removeObject(message);
            }
        });
    },
    saveMessage: function(message) {
        console.debug("%@.saveMessage".fmt(this), message);
        $.ajax({
            url: '/_api/message/%@'.fmt(message.get('id')),
            contentType: 'application/json',
            data: JSON.stringify(message.getData()),
            type: "PUT"
        });
    },
    updateMessage: function(message) {
        console.debug("%@.updateMessage".fmt(this), message);
        App.socket.send({
            key: "watercooler.message.updating",
            data: message.getData()
        });
    }
});


App.ApplicationController = Ember.Controller.extend({
//    init: function() {
//        console.debug("%@.init".fmt(this));
//        this._super();
//    },
    accountBinding: 'App.authController.account'
});


App.ChatController = Ember.ArrayController.extend({
//    init: function() {
//        console.debug("%@.init".fmt(this));
//        this._super();
//    },
    accountBinding: 'App.authController.account',
    contentBinding: 'App.messageListController.content'
});

App.ChatRoute = Ember.Route.extend({
//    init: function() {
//        console.debug("%@.init".fmt(this));
//        this._super();
//    },
    setupControllers: function() {
//        console.debug("%@.setupControllers".fmt(this));
        App.authController.signIn();
    }
});


App.MessageController = Ember.ObjectController.extend({
    init: function() {
//        console.debug("%@.init".fmt(this));
        this._super();
        this.set('content', null);
    }
});

App.MessageRoute = Ember.Route.extend({
//    init: function() {
//        console.debug("%@.init".fmt(this));
//        this._super();
//    },
    setupControllers: function(controller, message) {
//        console.debug("%@.setupControllers".fmt(this), controller, message);
        App.authController.signIn();
        controller.set('content', message);
    }
});


App.MessageTimeView = Ember.View.extend({
    tagName: 'time',
    attributeBindings: ['datetime'],
    didInsertElement: function() {
//        console.debug("%@.didInsertElement".fmt(this));
        this.$().timeago();
    },
    datetime: function() {
        return this.get('message.timeISOText');
    }.property('message.timeISOText')
});


App.AccountField = Ember.TextField.extend({
    init: function() {
        this._super();
        this.accountChanged();
    },
    accountBinding: 'App.authController.account',
    maxlength: 50,
    placeholderBinding: 'account.number',
    accountChanged: function() {
        var name = this.get('account.name');
        this.setProperties({
            name: name,
            value: name
        });
    }.observes('account'),
    disabled: function() {
        return !App.get('socket.isConnected');
    }.property('App.socket.isConnected'),
    keyDown: function(e) {
//        console.debug("%@.keyDown".fmt(this), e);
        if (27 === e.which) {
            var account = this.get('account'),
                name = this.get('name');
            account.set('name', name);
            this.set('value', name);
            App.authController.updateAccount(account);
        }
    },
    keyUp: function(e) {
//        console.debug("%@.keyUp".fmt(this), e);
        if (13 === e.which) {
            return;
        }
        var account = this.get('account');
        account.set('name', this.get('value'));
        App.authController.updateAccount(account);
    },
    _save: function(e) {
//        console.debug("%@._save".fmt(this), e);
        if (this.get('name') === this.get('value')) {
            return;
        }
        var account = this.get('account');
        this.set('name', account.get('name'));
        App.authController.saveAccount(account);
        this.$().blur();
    },
    focusOut: function(e) {
//        console.debug("%@.focusOut".fmt(this), e);
        this._save(e);
    },
    insertNewline: function(e) {
//        console.debug("%@.insertNewline".fmt(this), e);
        this._save(e);
    }
});

App.MessageField = Ember.TextArea.extend({
    message: null,
    placeholder: "...",
    messageListBinding: 'App.messageListController.content',
    disabled: function() {
        return !App.get('socket.isConnected');
    }.property('App.socket.isConnected'),
    keyDown: function(e) {
//        console.debug("%@.keyDown".fmt(this), e);
        if (27 === e.which) {
            var message = this.get('message');
            this.set('message', null);
            this.set('value', null);
            if (message) {
                App.messageListController.deleteMessage(message);
            }
        }
    },
    keyUp: function(e) {
//        console.debug("%@.keyUp".fmt(this), e);
        if (13 === e.which) {
            return;
        }
        var messageText = this.get('value'),
            message = this.get('message');
        messageText = messageText && messageText.trim();
        if (!messageText) {
            this.set('message', null);
            this.set('value', null);
            if (message) {
                App.messageListController.deleteMessage(message);
            }
            return;
        }
        if (message) {
            if (message.get('id')) {
                message.set('text', messageText);
                App.messageListController.updateMessage(message);
            }
        } else {
            message = App.messageListController.createMessage();
            message.set('text', messageText);
            this.set('message', message);
        }
    },
    _save: function(e) {
//        console.debug("%@._save".fmt(this), e);
        var message = this.get('message');
        if (! message) {
            return;
        }
        App.messageListController.saveMessage(message);
        this.set('value', null);
        this.set('message', null);
    },
    insertNewline: function(e) {
//        console.debug("%@.insertNewline".fmt(this), e);
        this._save(e);
    }
});


App.Router = Ember.Router.extend({
    location: 'history',
    rootURL: '/'
});


App.Router.map(function(match) {
    match('/').to('chat');
    match('/message/:message_id').to('message');
});
